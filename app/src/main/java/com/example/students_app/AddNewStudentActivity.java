package com.example.students_app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.example.students_app.model.Student;

public class AddNewStudentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_student);

        setSaveButtonAction();
        setCancelButtonAction();
    }

    private void setSaveButtonAction() {
        Button saveButton = findViewById(R.id.addNewStudent_save);
        saveButton.setOnClickListener(view -> {
            EditText nameEditTest = findViewById(R.id.addNewStudent_name_pt);
            EditText idEditTest = findViewById(R.id.addNewStudent_id_pt);
            EditText phoneEditTest = findViewById(R.id.addNewStudent_phone_pt);
            EditText addressEditTest = findViewById(R.id.addNewStudent_address_pt);
            CheckBox checkBox = findViewById(R.id.addNewStudent_cb);

            Student newStudent = new Student(nameEditTest.getText().toString(), idEditTest.getText().toString(),
                    "", phoneEditTest.getText().toString(), addressEditTest.getText().toString(), checkBox.isChecked());
            StudentsRecycleListActivity.data.add(newStudent);
            finish();
            Intent intent = new Intent(this, StudentsRecycleListActivity.class);
            startActivity(intent);
        });
    }

    private void setCancelButtonAction() {
        Button cancelButton = findViewById(R.id.addNewStudent_cancel);
        cancelButton.setOnClickListener(view -> finish());
    }
}