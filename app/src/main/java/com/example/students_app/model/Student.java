package com.example.students_app.model;

import java.io.Serializable;
import java.util.List;

public class Student implements Serializable {

    private String name;
    private String id;
    private String imageUrl;
    private String phone;
    private String address;
    private boolean cb;

    public Student(String name, String id, String imageUrl, String phone, String address, boolean cb) {
        this.name = name;
        this.id = id;
        this.imageUrl = imageUrl;
        this.phone = phone;
        this.address = address;
        this.cb = cb;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isCb() {
        return cb;
    }

    public void setCb(boolean cb) {
        this.cb = cb;
    }
}
