package com.example.students_app.model;

import java.util.LinkedList;
import java.util.List;

public class Model {

    static final Model instance = new Model();
    List<Student> data = new LinkedList<>();

    private Model() {
        for (int i = 0; i < 20; i++) {
            addStudent(new Student("student " + i, "12345678", "", "0541234567", "Tel Aviv", false));
        }
    }

    public static Model instance() {
        return instance;
    }

    public List<Student> getAllStudents() {
        return data;
    }

    public void addStudent(Student student) {
        data.add(student);
    }
}
