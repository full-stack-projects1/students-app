package com.example.students_app;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.example.students_app.model.Student;

public class EditStudentActivity extends AppCompatActivity {

    Student student;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_student);

        int pos = getIntent().getIntExtra("pos", 0);
        student = StudentsRecycleListActivity.data.get(pos);

        EditText name = findViewById(R.id.editStudent_name_pt);
        EditText id = findViewById(R.id.editStudent_id_pt);
        EditText phone = findViewById(R.id.editStudent_phone_pt);
        EditText address = findViewById(R.id.editStudent_address_pt);
        CheckBox checkBox = findViewById(R.id.editStudent_cb);

        setData(student, name, id, phone, address, checkBox);
        setCancelButtonAction();
        setDeleteButtonAction(pos);
        setSaveButtonAction(pos, name, id, phone, address, checkBox);
    }

    private void setData(Student student, EditText name, EditText id, EditText phone, EditText address, CheckBox checkBox) {
        name.setText(student.getName());
        id.setText(student.getId());
        phone.setText(student.getPhone());
        address.setText(student.getAddress());
        checkBox.setChecked(student.isCb());
    }

    private void setSaveButtonAction(int pos, EditText name, EditText id, EditText phone, EditText address, CheckBox checkBox) {
        Button saveButton = findViewById(R.id.editStudent_save);
        saveButton.setOnClickListener(view -> {
            Student editedStudent = StudentsRecycleListActivity.data.get(pos);
            editedStudent.setName(name.getText().toString());
            editedStudent.setId(id.getText().toString());
            editedStudent.setPhone(phone.getText().toString());
            editedStudent.setAddress(address.getText().toString());
            editedStudent.setCb(checkBox.isChecked());
            finish();
        });
    }

    private void setDeleteButtonAction(int pos) {
        Button deleteButton = findViewById(R.id.editStudent_delete);
        deleteButton.setOnClickListener(view -> {
            student.setId(null);
            finish();
        });
    }

    private void setCancelButtonAction() {
        Button cancelButton = findViewById(R.id.editStudent_cancel);
        cancelButton.setOnClickListener(view -> finish());
    }
}