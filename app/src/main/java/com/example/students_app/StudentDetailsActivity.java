package com.example.students_app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.students_app.model.Student;

public class StudentDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_details);

        int pos = getIntent().getIntExtra("pos", 0);
        setData(pos);
        setEditButtonAction(pos);
    }

    @Override
    protected void onResume() {
        super.onResume();
        int pos = getIntent().getIntExtra("pos", 0);
        setData(pos);
    }

    private void setData(int pos) {
        Student student = StudentsRecycleListActivity.data.get(pos);

        if (student.getId() == null){
            StudentsRecycleListActivity.data.remove(pos);
            finish();
        }

        TextView name = findViewById(R.id.studentDetails_student_name);
        TextView id = findViewById(R.id.studentDetails_student_id);
        TextView phone = findViewById(R.id.studentDetails_student_phone);
        TextView address = findViewById(R.id.studentDetails_student_address);
        CheckBox checkBox = findViewById(R.id.studentDetails_cb);

        name.setText(student.getName());
        id.setText(student.getId());
        phone.setText(student.getPhone());
        address.setText(student.getAddress());
        checkBox.setChecked(student.isCb());
    }

    private void setEditButtonAction(int pos) {
        Button editButton = findViewById(R.id.studentDetails_edit);
        editButton.setOnClickListener(view -> {
            Intent intent = new Intent(this, EditStudentActivity.class);
            intent.putExtra("pos", pos);
            startActivity(intent);
        });
    }
}