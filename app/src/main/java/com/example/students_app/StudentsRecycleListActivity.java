package com.example.students_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.students_app.model.Model;
import com.example.students_app.model.Student;

import java.util.List;

public class StudentsRecycleListActivity extends AppCompatActivity {
    public static List<Student> data;
    StudentRecycleAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students_recycle_list);

        data = Model.instance().getAllStudents();

        RecyclerView list = findViewById(R.id.studentRecycle_list);
        list.setHasFixedSize(true);
        list.setLayoutManager(new LinearLayoutManager(this));

        adapter = new StudentRecycleAdapter();
        list.setAdapter(adapter);

        adapter.setOnItemClickedListener(new OnItemClickedListener() {
            @Override
            public void onItemClick(View view, int pos) {
                Intent intent = new Intent(view.getContext(), StudentDetailsActivity.class);
                intent.putExtra("pos", pos);
                startActivity(intent);
            }
        });

        Button addUserButton = findViewById(R.id.studentRecycle_button);
        addUserButton.setOnClickListener(view -> {
            Intent intent = new Intent(this, AddNewStudentActivity.class);
            startActivity(intent);
        });
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    class StudentViewHolder extends RecyclerView.ViewHolder {
        TextView studentName;
        TextView studentId;
        CheckBox cb;

        public StudentViewHolder(@NonNull View itemView, OnItemClickedListener listener) {
            super(itemView);
            studentName = itemView.findViewById(R.id.studentslistrow_student_name);
            studentId = itemView.findViewById(R.id.studentslistrow_student_id);
            cb = itemView.findViewById(R.id.studentslistrow_checkBox);

            cb.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                      int pos = (int) cb.getTag();
                      Student student = data.get(pos);
                      student.setCb(cb.isChecked());
                  }
              }
            );

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = getAdapterPosition();
                    listener.onItemClick(view, pos);
                }
            });
        }

        public void bind(Student student, int pos) {
            studentName.setText(student.getName());
            studentId.setText(student.getId());
            cb.setChecked(student.isCb());
            cb.setTag(pos);
        }
    }

    public interface OnItemClickedListener {
        void onItemClick(View view, int pos);
    }

    class StudentRecycleAdapter extends RecyclerView.Adapter<StudentViewHolder> {
        OnItemClickedListener listener;

        public void setOnItemClickedListener(OnItemClickedListener listener) {
            this.listener = listener;
        }

        @NonNull
        @Override
        public StudentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            @SuppressLint("InflateParams") View view = getLayoutInflater().inflate(R.layout.students_list_row, parent, false);
            return new StudentViewHolder(view, listener);
        }

        @Override
        public void onBindViewHolder(@NonNull StudentViewHolder holder, int position) {
            Student student = data.get(position);
            holder.bind(student, position);
        }

        @Override
        public int getItemCount() {
            return data.size();
        }
    }
}